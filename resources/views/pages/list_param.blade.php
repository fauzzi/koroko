@extends('app')
@section('content')
		<div class="row">
			<div class="col-sm-3">
				<div class="sidebar-module col-sm-offset-1  ">
				<ul>						
					@for ($i = 0; $i < count($unique); $i++)						
						<li><a href="{{urlencode($unique[$i])}}"> {{$unique[$i]}} </a></li>					   
					@endfor
				</ul>	
				</div>
			</div>

			<div class="col-sm-8">			
					<h3>Here your result with : {{$param}}</h3>
				@foreach ($data as $result)					
					<div class="row">
						<div class="col-xs-6 col-md-2">
							<a href="#" class="thumbnail">
								<img style="width:100px;height:100px;" src="{{$result->profile_image_url}}" alt="{{$result->screen_name}}">
							</a>
						</div>
					<div class="col-xs-12 col-md-10">
						<h2> {{$result->name}} 
							<small>
							<a target="_blank" href="https://twitter/<?php echo $result->screen_name ?>">{{$result->screen_name}}</a>
							</small>
						</h2>
					<p>
						{{$result->text}}
					</p>
					<p>
						<?php echo $result->source ?>
					</p>
					<p>
						Retweet Count{{$result->retweet_count}} | Created At {{$result->created_at}}
					</p>
					</div>
					</div>
				@endforeach			
			</div>
		</div>						
@stop