<?php
	$result = (array)json_decode($records);
	$count = count($result['statuses']);	
?>

<div class="container">
	<?php for($i=0;$i<$count;$i++){ ?>
		<div class="row">
				  
		  <div class="col-xs-6 col-md-2">
		    <a href="#" class="thumbnail">
		      <img style="width:150px;height:150px;" src="<?php echo $result['statuses'][$i]->user->profile_image_url_https ?>">
		    </a>
		  </div>

		  <div class="col-xs-12 col-md-10">		  	
			  <h2> <?php echo $result['statuses'][$i]->user->name ?><small>
			  	<a target="_blank" href="https://twitter/<?php echo $result['statuses'][$i]->user->screen_name ?>">
			  		<?php echo $result['statuses'][$i]->user->screen_name ?></a></small>
			  	</h2>
			  	<p>
			  		<?php echo $result['statuses'][$i]->text ?>
			  	</p>
			  	<p>
			  		<?php echo $result['statuses'][$i]->source ?>
			  	</p>
			  	<p>
			  	 Retweet Count : <?php echo $result['statuses'][$i]->retweet_count ?>			  	 

				<form id="formStore" method="post" action="/store">
					<input id="category" type="hidden" name="category" value="<?php echo $search ?>">
					<input type="hidden" name="name" value="<?php echo $result['statuses'][$i]->user->name ?>">
					<input type="hidden" name="screen_name" value="<?php echo $result['statuses'][$i]->user->screen_name ?>">
					<input type="hidden" name="profile_image_url" value="<?php echo $result['statuses'][$i]->user->profile_image_url_https ?>">
					<input type="hidden" name="text" value="<?php echo $result['statuses'][$i]->text ?>">
					<input type="hidden" name="source" value="<?php echo htmlentities($result['statuses'][$i]->source) ?>">
					<input type="hidden" name="retweet_count" value="<?php echo $result['statuses'][$i]->retweet_count ?>">
					<button type="submit" class="btn btn-default col-sm-2">Store</button> 			 	
				</form>
			  	</p>			  				  
		  </div>
		</div>
		<?php } ?>
	</div>

	@section('footer')
		<script type="text/javascript">
			$(document).ready(function(){
				 // var cat = $('#category').val();
				 // window.location.href = "q?search=" + encodeURIComponent(cat);
				//  $('form#formStore').bind('submit', function(){
				//  	document.location.href = "q?search=" + encodeURIComponent(cat);
				//  	return false;
				//  });
			});					
		</script>
	@stop