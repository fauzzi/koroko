@extends('app')
@section('content')
	<article id="mainContent">	
		<h3>Here your result with : {{$search}}</h3>
			@if (count($records)===0)
				<p>No search found</p>
			@else
				@include('pages.partial_search')	
			@endif		   		
	</article>	
@stop