<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="{{ elixir('css/all.css') }}">
</head>
<body>
	<div class="container">		
		@include('partials.nav')
		
		@include('flash::message')
		
		@yield('content')
	</div>	

	<script src="{{ elixir('js/all.js') }}" type="text/javascript"></script>

	@yield('footer')
</body>
</html>