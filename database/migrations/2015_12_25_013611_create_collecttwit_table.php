<?php

use Jenssegers\Mongodb\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCollecttwitTable extends Migration
{
    /**
     * Run the migrations.
     *          'category', 'name','screen_name','profile_image_url','text','source','retweet_count',
     * @return void
     */
    public function up()
    {
        Schema::create('collecttwit', function (Blueprint $collection) {
            $collection->increments('id');
            $collection->string('category');
            $collection->string('name');
            $collection->string('screen_name');
            $collection->string('profile_image_url');
            $collection->string('text');            
            $collection->string('source');
            $collection->integer('retweet_count');
            $collection->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('collecttwit');
    }
}
