<?php

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;


class CollectTwit extends Eloquent
{
	protected $connection = 'mongodb';
	protected $collection = 'collecttwit';
	
        protected $fillable = [
        'category', 'name','screen_name','profile_image_url','text','source','retweet_count',
    ];
}
