<?php


Route::get('/', function () {
	
    return view ('app');
});

Route::get('q', 'CollectTwitController@setSearch');

Route::post('store', 'CollectTwitController@setStore');

Route::get('list', 'CollectTwitController@getList');

Route::get('{id}', 'CollectTwitController@getParam');