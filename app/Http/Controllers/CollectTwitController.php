<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Thujohn\Twitter\Facades\Twitter;
use App\CollectTwit;
use App;


class CollectTwitController extends Controller
{


    public function setSearch(Request $request){            	
        $search = $request->search;                    
        $query = array(
          "q" => $search, // bisa #jogja atau @jogja
          "count"=>20,
          'format'=>'json'
        );

        $records = Twitter::getSearch($query);        
        // echo "<pre>";
        // dd(json_decode($records));
        // echo "</pre>";
        return view('pages.search', compact('records','search'));
    }

    public function setStore(Request $request){        
        $data = array(
            'category'=> $request->category,
            'name'=> $request->name,
            'screen_name'=> $request->screen_name,
            'profile_image_url'=> $request->profile_image_url,
            'text'=> $request->text,
            'source' => $request->source,
            'retweet_count'=> $request->retweet_count
        );
        
         $d = CollectTwit::create($data);

         flash()->success('Your result has been created!');
           // Flash::message('Your result has been created!');
         // flash()->overlay('Your article has been successfully created!', 'Good Job');
         return back()->withInput();         
    }

    public function getList(){
        $col = CollectTwit::all();
        $data = json_decode($col->toJson());

        $category = array();
        foreach ($data as $key => $value) {
            $category[$key] = $value->category;
        }

        //collect unique categories
        $unique = array_keys(array_flip($category));

        return view('pages.list_store', compact('unique'));
    }

    public function getParam($id){    

        $col = CollectTwit::where('category','=', urldecode($id))->get();
        $data = json_decode($col->toJson());

        $all = CollectTwit::all();
        $json = json_decode($all->toJson());

        $category = array();
        foreach ($json as $key => $value) {
            $category[$key] = $value->category;
        }

        //collect unique categories
        $unique = array_keys(array_flip($category));
        $param  = urldecode($id);
        return view('pages.list_param', compact('data','unique','param'));
    }


}